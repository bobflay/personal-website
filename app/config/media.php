<?php

// return [

//     'photos' => [

//         'presets' => [
//             'small'     => ['width' => 350, 'height' => 200],
//             'thumbnail' => ['width' => 300, 'height' => 200],
//             'square'    => ['width' => 200, 'height' => 200]
//         ]
//     ]

// ];
 
 
return [

    'photos' => [

        'presets' => [
            'small'     => ['width' => 150, 'height' => 100],
            'thumbnail' => ['width' => 350, 'height' => 233],
            'square'    => ['width' => 50, 'height' => 50]
        ]
    ],

    'location' => 'cms/images/uploads'
];
