<?php

return [
    's3' => [

        'bucket' => 'awsfacebookapp',

        'credentials' => ['key'    => getenv('aws_key'), 'secret' => getenv('aws_secret')]

    ]
];