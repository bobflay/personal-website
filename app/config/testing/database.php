<?php

return [

    'default' => 'mysql',

    'connections' => [

        'mysql' => [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]
    ]
];
