<?php

use Agency\Cache\PostCacheManager;
use Agency\Contracts\Cms\Repositories\SectionRepositoryInterface;
use Agency\Contracts\Repositories\PostRepositoryInterface;
use Agency\Api\Mappers\PostMapper;
use Vinelab\Http\Client as HttpClient;
use Illuminate\Mail\Mailer;



class PullMediaController extends Controller {

	public function __construct(PostCacheManager $cache,
                                SectionRepositoryInterface $section,
                                PostRepositoryInterface $post,
                                Mailer $mailer)
	{
		$this->cache = $cache;
        $this->section = $section;
        $this->post = $post;
        $this->post_mapper = new PostMapper();
        $this->http=App::make('Vinelab\Http\Client');
        $this->code = 'V5BIQ2E5ZlafXtn33UREHRXCWotR9xSV0PMsRMb2';
        $this->mailer = $mailer;

	}

	public function index()
	{
		$request = [
	        'url' => 'http://api.agency.app/posts/main-project',
	        'params' => [

	            'code'     => $this->code
	        ]
    	];

    	$projects = $this->http->get($request)->json();


    	$request = [
	        'url' => 'http://api.agency.app/posts',
	        'params' => [

	            'code'     => $this->code,
	            'category' => 'presentation'
	        ]
    	];

    	$presentation = $this->http->get($request)->json();


		return View::make('pmedia.index',compact('projects','presentation'));
	}

	public function web()
	{
		$request = [
	        'url' => 'http://api.agency.app/posts',
	        'params' => [

	            'code'     => $this->code,
	            'category' => 'web'
	        ]
    	];

    	$projects = $this->http->get($request)->json();

    	return View::make('pmedia.projects',compact('projects'));

	}


	public function mobile()
	{
		$request = [
	        'url' => 'http://api.agency.app/posts',
	        'params' => [

	            'code'     => $this->code,
	            'category' => 'mobile'
	        ]
    	];

    	$projects = $this->http->get($request)->json();

    	return View::make('pmedia.projects',compact('projects'));

	}

	public function careers()
	{

	}

	public function contact()
	{
		
	}




	public function show($id)
	{
		$request = [
	        'url' => 'http://api.agency.app/posts/'.$id,
	        'params' => [

	            'code'     => $this->code
	        ]
    	];	

    	$post = $this->http->get($request)->json();

    	return View::make('me.modal',compact('post'));
	}

	public function email()
	{
		try {

			$name = Input::get('name');
			$email = Input::get('email');
			$phone = Input::get('phone');
			$text = Input::get('message');

			$this->mailer->send('me.email', compact('name','email', 'phone', 'text'), function($message){
            	$message->to('bob.fleifel@gmail.com', 'ibrahimfleifel.com')->subject('A New Message');
        });
			
		} catch (Exception $e) {
			dd($e);
		}
		
	}

}

