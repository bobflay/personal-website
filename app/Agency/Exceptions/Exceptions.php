<?php namespace Agency\Exceptions;

/**
 * @author Abed Halawi <abed.halawi@vinelab.com>
 */

use RuntimeException;

class AgencyException extends RuntimeException {

    /**
     * The error messages to pass.
     *
     * @var array
     */
    protected $messages;

    public function __construct($messages = array())
    {
        // add support for sending in
        // one single message as an array
        if ( ! is_array($messages))
        {
            $messages = [$messages];
        }

        $this->messages = $messages;
    }

    public function messages()
    {
        return $this->messages;
    }
}

class InvalidImageException extends AgencyException {}

class InvalidPostException extends AgencyException {}

class InvalidTagException extends AgencyException {}

class InvalidSectionException extends AgencyException {}

class InvalidVideoException extends AgencyException {}

class PostNotFoundException extends AgencyException {}

class InvalidRoleException extends AgencyException {}

class InvalidPermissionException extends AgencyException {}

