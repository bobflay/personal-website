<div class="row">
	<div class="twelve columns">
		<div class="centersectiontitle">
			<h4>PRÉSENTATION</h4>
		</div>
	</div>

	@foreach($presentation->data as  $key=>$post)
			@if($key!=0)
				<div class="four columns">
					<h5>{{$post->title}}</h5>
					<div class="portofoliothumb">
						<img src="{{$post->cover->thumbnail}}" alt="">
					</div>
					<p>
						<a href="#" class="readmore">Learn more</a>
					</p>
				</div>
			@endif
			
	@endforeach
</div>
<div class="hr">
</div>