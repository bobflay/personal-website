<div id="ei-slider" class="ei-slider">
	<ul class="ei-slider-large">
		<li>
			<img src="{{$projects->data->cover->original}}" alt="image01" class="responsiveslide">
			<div class="ei-title">
				<h2>PROJETS</h2>
				<h3>PHARES</h3>
			</div>
		</li>
		@foreach($projects->data->images as $image)
			<li>
				<img src="{{$image->original}}" alt="image01" class="responsiveslide">
				<div class="ei-title">
					<h2>PROJETS</h2>
					<h3>PHARES</h3>
				</div>
			</li>
		@endforeach

	</ul>
	<!-- slider-thumbs -->
	<ul class="ei-slider-thumbs">
		<li class="ei-slider-element">Current</li>
		<li><a href="#"></a><img src="{{$projects->data->cover->original}}" class="slideshowthumb" alt="thumb01"/></li>
		@foreach($projects->data->images as $image)
			<li><a href="#"></a><img src="{{$image->thumbnail}}" class="slideshowthumb" alt="thumb01"/></li>
		@endforeach
	</ul>	
</div>
<div class="minipause">
</div>