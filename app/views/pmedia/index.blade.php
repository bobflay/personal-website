<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
	@include('pmedia.head')
<body>
<!-- HEADER
================================================== -->
	@include('pmedia.navigation')
<!-- SLIDER 
================================================== -->
	@include('pmedia.slider')
<!-- SUBHEADER
================================================== -->
	@include('pmedia.subheader')
<!-- ANIMATED COLUMNS 
================================================== -->
	@include('pmedia.columns')
<!-- CONTENT 
================================================== -->
	@include('pmedia.content')

<!-- TWITTER
================================================== -->
	@include('pmedia.twitter')
<!-- FOOOTER 
================================================== -->
	@include('pmedia.footer')

<div class="copyright">
	<div class="row">
		<div class="six columns">
			 &copy;<span class="small">Pull Media</span>
		</div>

</div>
	@include('pmedia.scripts')
</body>
</html>