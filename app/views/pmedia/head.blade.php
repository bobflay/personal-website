<head>
	<meta charset="utf-8"/>
	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width"/>
	<title>Pull Media</title>
	<!-- CSS Files-->
	<link rel="stylesheet" href="/pull-media/stylesheets/style.css">
	<link rel="stylesheet" href="/pull-media/stylesheets/homepage.css"><!-- homepage stylesheet -->
	<link rel="stylesheet" href="/pull-media/stylesheets/skins/teal.css"><!-- skin color -->
	<link rel="stylesheet" href="/pull-media/stylesheets/responsive.css">
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	  <![endif]-->    
</head>