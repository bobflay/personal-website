<div class="row">
</div>
<div id="portofolio" class="row">
	<!-- Project 1-->
	@foreach($projects->data as $project)
		<div class="six columns category trains">
			<h5>{{$project->title}}</h5>
			<p>
				 {{$project->body}}
			</p>
			<div class="portofoliothumb">
				<div class="portofoliothumboverlay">
					<div class="viewgallery">
						<!--image here -->
						<a data-gal="prettyPhoto[gallery]" href="/posts/{{$project->slug}}"><img src="{{$project->cover->original}}" class="left galleryicon" alt=""></a>
					</div>
					<div class="inner">
						<a class="projectdetail" href="/posts/{{$project->slug}}">+ Project Details</a>
					</div>
				</div>
				<!-- image here -->
				<img src="{{$project->cover->original}}" alt="">
			</div>
		</div>
	@endforeach

</div>
<div class="hr">
</div>