@include('me.head')
@include('me.scripts')

@if(isset($post))
    <div class="portfolio-modal" id="{{$post->data->slug}}" tabindex="-1" role="dialog" aria-hidden="true">
        <meta property="og:title" content="{{$post->data->title}}" />
        <div class="modal-content">
            <div class="close-modal" onclick="home()">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>{{$post->data->title}}</h2>
                            <img class="img-responsive img-centered" src="{{$post->data->cover->original}}" alt="">
                            <p>{{$post->data->body}}</p>
                            <button type="button" onclick="home()"  class="btn btn-primary"> <i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif



<script type="text/javascript">
	function home()
	{
		document.location.href="/";
	}
</script>

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57431313-1', 'auto');
  ga('send', 'pageview');

</script>