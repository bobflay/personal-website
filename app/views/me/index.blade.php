<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

@include('me.head')

<body id="page-top" class="index">

    @include('me.navigation')

    @include('me.header')


    @include('me.news')

    @include('me.portfolio')


    @include('me.about')

    @include('me.team')

    @include('me.contact')


    @include('me.footer')


    <!-- Portfolio Modals -->
    <!-- Use the modals below to showcase details about your portfolio projects! -->

    @include('me.scripts')

</body>

</html>
