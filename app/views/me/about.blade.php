    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        @if(sizeof($about->data)>0)
                            @foreach ($about->data as $key => $post)
                                <li class="{{$key%2==0?'':'timeline-inverted'}}">
                                    <div class="timeline-image">
                                        <img class="img-circle img-responsive" src="{{$post->cover->original}}" alt="">
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4>{{$post->tags[0]->text}}</h4>
                                            <h4 class="subheading">{{$post->title}}</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p class="text-muted">{{$post->body}}</p>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @endif
   
                    </ul>
                </div>
            </div>
        </div>
    </section>