 <!-- Team Section -->
    <section id="team" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Amazing People</h2>
                    <h3 class="section-subheading text-muted">People that influenced me</h3>
                </div>
            </div>
                @if(sizeof($people->data)>0)
                    <ul id="lists">
                        @foreach ($people->data as $key => $post)

                            <div class="col-sm-4">
                                <div class="team-member">
                                    <img src="{{$post->cover->original}}" class="img-responsive img-circle" alt="">
                                    <h4>{{$post->title}}</h4>
                                    <p class="text-muted">{{$post->body}}</p>
                                    <ul class="list-inline social-buttons">
                                        <li><a href="{{$post->tags[0]->text}}"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li><a href="{{$post->tags[1]->text}}"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li><a href="{{$post->tags[2]->text}}"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        @endforeach
                    </ul>
                @endif
        </div>
    </section>
