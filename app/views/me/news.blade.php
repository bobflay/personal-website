
    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">News</h2>
                    <h3 class="section-subheading text-muted">The Latest Tech News</h3>
                </div>
            </div>

            
                @foreach ($news->data as $post)
                   
                        <a href="/posts/{{$post->slug}}" class="portfolio-link" data-toggle="modal">
                                <div class="col-md-4">
                                        <div class="portfolio-hover">
                                            <div class="portfolio-hover-content">
                                            </div>
                                        </div>
                                        <img src="{{$post->cover->thumbnail}}" class="img-responsive" alt="">                                
                                    <h4 class="service-heading">{{substr($post->title,0,57)}}...</h4>
                                    <h3 class="section-subheading text-muted">{{$post->publish_date}}</h3>

                                </div>
                        </a>
                   

                @endforeach


            

        </div>
    </section>
