<body>
    <h1>{{Lang::get('email.welcome_to_agency')}}!</h1>
    <br /><br />
    {{Lang::get('email.use_this_password_to_login')}}: <br>
    <h3>{{$password }}</h3>
</body>
