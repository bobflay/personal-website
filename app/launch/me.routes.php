<?php

Route::get('/','MeController@index');

Route::get('/posts/{id}','MeController@show');

Route::post('/message', 'MeController@email');
