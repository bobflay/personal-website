<?php
Route::get('/','PullMediaController@index');
Route::get('/web','PullMediaController@web');
Route::get('/mobile','PullMediaController@mobile');
Route::get('/careers','PullMediaController@careers');
Route::get('/contact','PullMediaController@contact');
