<?php 

return [

    'title'         =>  'العنوان',
    'content'       =>  'الأقسام',
    'add_images'   	=>  'الصور',
    'add_videos'    =>  'الفيديو',
    'add_photo'     =>  'إضافة صور',
    'add_yt_btn'	=>	'إضافة',
    'yt_video_url'	=>	'إضافة يوتيوب فيديو',
    'editing'       =>  'تحرير',
    'publish'       =>  'نشر',
    'schedule'     =>  'جدول',
    'published'     =>  'نشر',
    'submit' 		=>	"حفظ",
    'conf_messasge_delete_post' => 'هل أنت متأكد من حذف هذا البوست؟',
    'empty_post_msg' => "لا يمكنك حفظ بوست فارغ ",
    'cancel'        => 'إلغاء',
    'delete' => 'حذف',
    'ok' => 'موافق',
    'invalid_youtube_url' => "الرجاء إدخال رابط يوتيوب صحيح",
    'title_error' => 'الرجاء كتابة عنوان',
    'new_post' => 'بوست جديد',
    'publish_date' => 'تاريخ النشر',
    'scheduled_at' => "جدول بتاريخ",
    'author' => "الكاتب",
    'edit' => 'تحرير',
    'agency_cms'=> "Agency CMS",
    'tag_input' => "أضف تاغ",
    'preview' => 'معاينة'
];
