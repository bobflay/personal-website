<?php 

return [
	'reset_password_body' => 'To reset your password go to this link',
	'click_here' => 'Click Here',
	'welcome' => 'Welcome',
	'welcome_to_agency' => 'Welcome to Agency',
	'use_this_password_to_login' => "use this password to login"

];