<?php 

return [
	'reset_password' => 'Reset Password',
	'enter_your_new_password' => 'Enter your new password',
	'new_password' => 'New Password',
	'retype_new_password' => 'Retype New Password',
	'password_updated_successfully' => 'Password Updated Successfully',
	'password_does_not_match_the_confirm_password' => 'Password does not match the confirm password',
	'check_ur_mail_msg' => 'An email has been sent to you, please check your inbox!',
	'change_password' => 'Change Password',
	'old_password' => "Old Password",
	'save' => "Save",
	'cancel' => "Cancel",
	'current_password_error' => "Your current password is not correct",
	'new_password_cannot_be_empty' => "New password cannot be empty",

];